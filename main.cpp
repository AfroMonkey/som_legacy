#include "som.cpp" // TODO change to .h

#include <iostream>

using namespace std;

#define ARCH 10

vector<COORD> gni(size_t x, size_t y) {
    vector<COORD> ni;
    ni.push_back(make_pair(x, y));
    if (y > 0) {
        ni.push_back(make_pair(x, y - 1));
    }
    if (x > 0) {
        ni.push_back(make_pair(x - 1, y));
    }
    if (y < ARCH - 1) {
        ni.push_back(make_pair(x, y + 1));
    }
    if (x < ARCH - 1) {
        ni.push_back(make_pair(x + 1, y));
    }
    return ni;
}

int main() {
    SOM<ARCH> som(gni);
    cout << &som.get_bmu({0, 0, 0, 0, 0, 0, 0, 0, 0, 0}) << endl;
    som.train({{1, 1, 1, 1, 1, 1, 1, 1, 1, 1}}, 1);
    cout << &som.get_bmu({0, 0, 0, 0, 0, 0, 0, 0, 0, 0}) << endl;
    return 0;
}
