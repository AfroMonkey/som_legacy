#ifndef SOM_H
#define SOM_H

#include "neuron.cpp"

#include <functional>
#include <utility> // pair
#include <vector>

#define COORD std::pair<size_t, size_t>

template<size_t N>
class SOM {
public:
    Neuron<N> neurons[N][N];

    SOM(std::function<std::vector<COORD>(size_t, size_t)> gni);

    Neuron<N> &get_bmu(const double (&input)[N]);

    void train(const double (&inputs)[][N], size_t inputs_size, double learning_rate = 0.5, size_t max_iter = 100);
};

#endif // SOM_H
