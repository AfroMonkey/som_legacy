#include "som.h"

template<size_t N>
SOM<N>::SOM(std::function<std::vector<COORD>(size_t, size_t)> gni) {
    for (size_t y = 0; y < N; ++y) {
        for (size_t x = 0; x < N; ++x) {
            for (auto i : gni(x, y)) {
                neurons[x][y].neighbors.push_back(&neurons[i.first][i.second]);
            }
        }
    }
}

template<size_t N>
Neuron<N> &SOM<N>::get_bmu(const double (&input)[N]) {
    COORD min = COORD(0, 0);
    for (size_t y = 0; y < N; ++y) {
        for (size_t x = 0; x < N; ++x) {
            if (neurons[x][y].get_distance(input) < neurons[min.first][min.second].get_distance(input)) {
                min = COORD(x, y);
            }
        }
    }
    return neurons[min.first][min.second];
}

template<size_t N>
void SOM<N>::train(const double (&inputs)[][N], size_t inputs_size, double learning_rate, size_t max_iter) {
    for (size_t s = 0; s < max_iter; ++s) {
        for (size_t i = 0; i < inputs_size; ++i) { // TODO maybe change to get random input
            for (auto neighbor : get_bmu(inputs[i]).neighbors) {
                neighbor->update_weights(inputs[i], learning_rate);
            }
        }
    }
}
