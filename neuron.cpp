#include "neuron.h"

#include <algorithm> // generate
#include <cmath>
#include <random> //random_device, mt19937, uniform_real_distribution

template<size_t N>
Neuron<N>::Neuron()
  : n(N) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(-1, 1);
    std::generate(weights, weights + N, [&]() -> double { return randDistrib(gen); });
}

template<size_t N>
double Neuron<N>::get_distance(const double (&input)[N]) const {
    double sum = 0;
    for (size_t i = 0; i < n; ++i) {
        sum += (input[i] - weights[i]) * (input[i] - weights[i]);
    }
    return sqrt(sum);
}

template<size_t N>
std::string Neuron<N>::to_string() const {
    std::string s;
    for (size_t i = 0; i < n; ++i) {
        s += std::to_string(weights[i]) + " ";
    }
    s += "\n";
    for (auto neighbor : neighbors) {
        s += std::to_string(neighbor->n) + " ";
    }
    return s;
}

template<size_t N>
void Neuron<N>::update_weights(const double (&input)[N], double learning_rate) {
    for (size_t i = 0; i < n; ++i) {
        weights[i] += learning_rate * (input[i] - weights[i]);
    }
}
