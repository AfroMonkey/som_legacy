#ifndef NEURON_H
#define NEURON_H

#include <string>
#include <vector>

template<size_t N>
class Neuron {
public:
    std::vector<Neuron *> neighbors;
    const size_t n;
    double weights[N];

    Neuron();
    double get_distance(const double (&input)[N]) const;
    std::string to_string() const;
    void update_weights(const double (&input)[N], double learning_rate);
};

#endif // NEURON_H
